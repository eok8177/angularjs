angular.module('customerApp', [])
// Should separate this page to 2 controller

// The controller to manage the list
  .controller('CustomersController', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope) {

    $rootScope.customerKey = 0;
    $rootScope.table = {
      head: ["No","ID","Name","TypeID","Created at","value 1","Updated at","value 2",],
      body: {}
    };

    // GET request
    $http({
      method: 'GET',
      url: 'https://data.austintexas.gov/api/views/aw6n-x665/rows.json?accessType=DOWNLOAD'
    }).then(function successCallback(response) {
      // Parse data
        angular.forEach(response.data.data, function(value, key) {
          var row = {};
          angular.forEach(value, function(value, key) {
            row[key] = value;
          });
          $rootScope.table.body[key] = row;
          $rootScope.customerKey = key;
        });
        $rootScope.status = response.status;

      }, function errorCallback(response) {
        console.log(status);
        console.log("Error occured");
      });

  }])

//  The controller to manage the form, include form validation
  .controller('NewCustomerController', ['$scope', '$rootScope', function($scope, $rootScope) {

    $scope.insert = function() {

      //Custom validation example
      if (!angular.isNumber(this.type)) {
        this.message = 'type must be a number';
        return;
      }


      //insert record into table
      $rootScope.customerKey++;
      $scope.table.body[$rootScope.customerKey] = {
        0: ($rootScope.table.body[($rootScope.customerKey-1)][0]+1),
        1: this.id,
        9: this.name,
        2: this.type,
        3: new Date(),
        5: new Date()
      };

      //empty form
      this.id = '';
      this.name = '';
      this.type = '';
    };

  }]);